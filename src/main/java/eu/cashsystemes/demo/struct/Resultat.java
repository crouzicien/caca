package eu.cashsystemes.demo.struct;

import java.util.List;

public class Resultat {
	private List<Compte> result;
	private String message;

	public Resultat(List<Compte> result, String message) {
		super();
		this.result = result;
		this.message = message;
	}

	public List<Compte> getResult() {
		return result;
	}

	public void setResult(List<Compte> result) {
		this.result = result;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
