package eu.cashsystemes.demo.struct;

import java.util.UUID;

public class Compte {
	private String _id;
	private String nom;
	private String prenom;

	public Compte(String nom, String prenom) {
		super();
		_id = UUID.randomUUID().toString();
		this.nom = nom;
		this.prenom = prenom;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String get_id() {
		return _id;
	}

	public void set_id(String _id) {
		this._id = _id;
	}
	
	

}
