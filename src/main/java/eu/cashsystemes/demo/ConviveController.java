package eu.cashsystemes.demo;

import java.util.ArrayList;
import java.util.List;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import eu.cashsystemes.demo.struct.Compte;
import eu.cashsystemes.demo.struct.Resultat;

@RestController
public class ConviveController {

	private static final List<Compte> convives = new ArrayList<>();

	@RequestMapping("/add")
	public Resultat add(@RequestBody Compte input) {
		System.out.println("==> nom: " + input.getNom());
		convives.add(input);
		return new Resultat(convives, convives.size() + " convive(s).");
	}

	@RequestMapping("/list")
	public Resultat list(@RequestParam(value = "name", defaultValue = "World") String name) {
		return new Resultat(convives, convives.size() + " convive(s).");
	}
}
